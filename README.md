# Fancy Blog

### Tech stack
* Adaptive.
  * S: 320px - 768px
  * M: 768px - 1200px
  * L: over 1200px 
* React
* React Router
* Mobile-first
* SaSS

### Demo
Live demo is hosted [here](https://kit-portfolio.gitlab.io/flexible-layouts/fancy-blog).

### Launch
To run the project follow the next steps:
* Install node modules
* Run `yarn start` or `npm run start`
* Enjoy

# Preview
![Project preview](preview.png#center)