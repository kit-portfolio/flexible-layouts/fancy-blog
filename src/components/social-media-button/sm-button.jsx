import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import './sm-button.scss';
import FSMButtons from '../pictograms/sm-buttons-factory';

export default function Smb(props) {
    const {title, link} = props.content;

    return (
        <Link to={link} className={`social-media-button smb-${title}-style`}>
            {FSMButtons(title)}
        </Link>
    );
}

Smb.propTypes = {
    content: PropTypes.shape({
        title: PropTypes.string.isRequired,
        link: PropTypes.string.isRequired
    })
}