import React from "react";
import {ReactComponent as DribbleIcon} from "./dribble.svg";
import {ReactComponent as FacebookIcon} from "./facebook.svg";
import {ReactComponent as PinterestIcon} from "./pinterest.svg";
import {ReactComponent as RSSIcon} from "./rss.svg";
import {ReactComponent as TwitterIcon} from "./twitter.svg";

const socialIcons = {
    dribble: DribbleIcon,
    facebook: FacebookIcon,
    pinterest: PinterestIcon,
    rss: RSSIcon,
    twitter: TwitterIcon
}

export default function FSMButtons(target, className = 'social-pictogram-default') {
    const Component = target in socialIcons ? socialIcons[target] : null;
    return Component ? <Component className={className} /> : null;
}
