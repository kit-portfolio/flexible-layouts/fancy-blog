import React from 'react';
import {Link} from "react-router-dom";
import PropTypes from "prop-types";
import { v4 as uuidv4 } from 'uuid';
import './post.scss';

export default function Post(props) {
    const {title, cover, link, desc, commentsQty, categories} = props.content;

    function renderCategories(categories) {
        return categories.map((item) => <Link to={item.link} className="post-category"
                                                  key={uuidv4()}>{item.title}</Link>);
    }

    function renderDesc(description) {
        return description.map((item) => <p className="post-description" key={uuidv4()}>{item}</p>);
    }

    return (
        <div>
            <img src={cover} alt="post cover" className="post-cover"/>
            <div className="post-info">
                <Link to={link} className="post-title">{title}</Link>
                <div className="post-meta">
                    <div className="post-category-box">
                        {renderCategories(categories)}
                    </div>
                    <Link to="/" className="post-comments">{`${commentsQty} comments`}</Link>
                </div>
                {renderDesc(desc)}
            </div>
        </div>
    );
}

Post.propTypes = {
    content: PropTypes.shape({
        title: PropTypes.string.isRequired,
        cover: PropTypes.string.isRequired,
        link: PropTypes.string.isRequired,
        desc: PropTypes.arrayOf(PropTypes.string).isRequired,
        commentsQty: PropTypes.number.isRequired,
        categories: PropTypes.arrayOf(
            PropTypes.shape({
                title: PropTypes.string.isRequired,
                link: PropTypes.string.isRequired,
            })).isRequired
    })
}
