import React from 'react';
import './theme/generic.scss';
import './theme/normalize.scss';
import Cover from "./sections/cover/cover";
import Navbar from "./sections/navbar/navbar";
import Posts from "./sections/posts/posts";
import Shots from "./sections/shots/shots";
import Footer from "./sections/footer/footer";

export default function App() {
    return (
        <div className="app">
            <Navbar/>
            <Cover/>
            <Posts/>
            <Shots/>
            <Footer/>
        </div>
    );
}
