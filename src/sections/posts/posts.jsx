import React, {useEffect, useState} from 'react';
import { v4 as uuidv4 } from 'uuid';
import './posts.scss';
import Post from "../../components/post/post";

export default function Posts() {
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        fetch('mocks/posts.json')
            .then(res => res.json())
            .then(data => setPosts(data.data))
    }, []);

    return (
        <section className="recent-posts-section">
            <h2 className="posts-header">our recent posts</h2>
            {posts.map((item, index) => {
                return <Post
                    content={item}
                    className={`post-box post-${++index}`}
                    key={uuidv4()}
                />
            })}
        </section>
    );
}
