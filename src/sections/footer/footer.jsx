import React, {useEffect, useState} from 'react';
import {v4 as uuidv4} from 'uuid';
import './footer.scss';
import Smb from "../../components/social-media-button/sm-button.jsx";

export default function Footer() {
    const [socialButtons, setSocialButtons] = useState([]);
    const [contacts, setContacts] = useState([]);

    useEffect(() => {
        fetch('mocks/social-buttons.json')
            .then(res => res.json())
            .then(data => setSocialButtons(data.data));
        fetch('mocks/contacts.json')
            .then(res => res.json())
            .then(data => setContacts(data.data));
    }, [])

    return (
        <footer className="footer-section">
            <h2 className="footer-header">get in touch with us</h2>
            <p className="footer-text">Maecenas faucibus molli interdum. Cras mattis consectetur purus sitor amet sed
                donec
                malesuada ullamcorper odio. </p>
            <div className="sm-buttons-container">
                {socialButtons.map((item) => <Smb content={item} key={uuidv4()}/>)}
            </div>
            {contacts.map(item =>
                <a
                    href={item.link}
                    className="footer-text"
                    target="_blank"
                    rel="noreferrer"
                    key={uuidv4()}>
                    {item.title}
                </a>)
            }
        </footer>
    );
}
