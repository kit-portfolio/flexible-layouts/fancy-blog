import React from 'react';

export default function Cover() {
    return (
        <section className="section-cover">
            <img
                src="img/covers/cover_s.jpg"
                srcset="img/covers/cover_m.jpg 768w, img/covers/cover_l.jpg 1200w"
                alt="cover"
                width="100%"
                height="auto"
            />
        </section>
    );
}
