import React, {useEffect, useState} from 'react';
import {Link} from "react-router-dom";
import {v4 as uuidv4} from 'uuid';
import clsx from 'clsx';
import './navbar.scss';
import {ReactComponent as MenuButton} from "../../components/pictograms/menu/menu-button.svg";
import {ReactComponent as CrossButton} from "../../components/pictograms/menu/cross-button.svg";

export default function Navbar() {
    const [navbarItems, setNavbarItems] = useState([]);
    const [isMenuOpen, setIsMenuOpen] = useState([false]);

    useEffect(() => {
        fetch('mocks/navbar-items.json')
            .then(res => res.json())
            .then(data => setNavbarItems(data.data))
    }, []);

    function renderMenuItems(navbarItems) {
        return navbarItems.map((item) => {
            if (item.enabled) {
                return <Link
                    to={item.link}
                    className="navbar-item"
                    key={uuidv4()}>
                    {item.value}</Link>
            } else {
                return null;
            }
        })
    }

    function handleMenuClick() {
        setIsMenuOpen(!isMenuOpen);
    }

    return (
        <section className="navigation-bar-section">
            <div className="hamburger-menu-container">
                <div className="hamburger-menu" onClick={() => handleMenuClick()}>
                    {isMenuOpen && <MenuButton id="hamburger-button" className="hamburger-menu-hamburger"/>}
                    {!isMenuOpen && <CrossButton id="cross-button" className="hamburger-menu-cross"/>}
                </div>
            </div>
            <nav id="navbar" className={clsx("navbar", (isMenuOpen) ? "navbar-closed" : null)} >
                {renderMenuItems(navbarItems)}
            </nav>
        </section>
    );
}
